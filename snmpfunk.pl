#!/usr/bin/perl -w

use lib "/usr/lib/mrtg2/";
use SNMP_util "0.89";
use SNMP_Session "0.57";        # needs map_table
#use Net::SNMP;


&snmpmapOID("MAC", "1.3.6.1.2.1.17.4.3.1.1");


$macf = 0;
if ($ARGV[2]) { $macf++; }

&snmpmapOID("ARP", "1.3.6.1.2.1.4.22.1.4");
#&snmpmapOID("ifDesc", 

$Cbase = "1.3.6.1.4.1.9.9.46.1.3.1.1";
&snmpmapOID("Cvlanlist", $Cbase.".2");
&snmpmapOID("Cvlanname", $Cbase.".4");

# vtpinstance.strange -> mac addr (@VLAN)
&snmpmapOID("MAC", "1.3.6.1.2.1.17.4.3.1.1");

@vlans = map (s/.*:(.*)/$1/, snmpwalk($DEV, "Cvlanlist"));

foreach (@vlans) {
    print "got vlan $_\n";

}

# strange@ -> bp   (@VLAN)
&snmpmapOID("Cvlan2bp", "1.3.6.1.2.1.17.4.3.1.2");

#unique bp
&snmpmapOID("Cbp2if","1.3.6.1.2.1.17.1.4.1.2");


@foo = snmpwalk($DEV, $OID);

print join ("\n", @foo);
print "\n";

sub hex2mac {
    my $hex = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC",$hex));
}

sub int2mac {
    my $int = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC",pack ("CCCCCC", split (/\./, $int))));
}
