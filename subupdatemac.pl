#!/usr/bin/perl -w

require "my_sql_qs.pl"

use DBI;

use lib "/usr/lib/mrtg2/";
use SNMP_util "0.89";
use SNMP_Session "0.57";        # needs map_table

&snmpmapOID("ARP", "1.3.6.1.2.1.4.22.1.2");
&snmpmapOID("MAC", "1.3.6.1.2.1.17.4.3.1.1");
&snmpmapOID("MACPORT", "1.3.6.1.2.1.17.4.3.1.2");
$Cbase = "1.3.6.1.4.1.9.9.46.1.3.1.1";
&snmpmapOID("Cvlanlist", $Cbase.".2");
&snmpmapOID("Cvlanname", $Cbase.".4");
&snmpmapOID("Cvlan2bp", "1.3.6.1.2.1.17.4.3.1.2");
&snmpmapOID("Cbp2if","1.3.6.1.2.1.17.1.4.1.2");
&snmpmapOID("WTF","1.3.6.1.2.1.1.1");

sub connect_dbh () {
    my $SQL_USER = "yes_plaintext_oh_no";
    my $SQL_PASSWORD = "yes_plaintext_oh_no";
    $SQL_CONNECT_STRING = "dbi:mysql:host=dev.MY_HOSTNAME.net;database=the_network_db";
    $dbh = DBI->connect ($SQL_CONNECT_STRING, $SQL_USER, $SQL_PASSWORD) || warn $DBI::errstr;
}

connect_dbh();

@switches = sqlQuery ($dbh, "switches", ["dev", "com" ]);
@routers = sqlQuery ($dbh, "routers", ["dev", "com" ]);


#GET INTERFACE IPS
##################
sqlFlush ($dbh, "interfaceips");

foreach (@routers, @switches) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};
    
    print "getting ipAddEntIfIndex descriptions from $dev\n";
    @ipa =  snmpwalk("$com\@$dev", "ipAdEntIfIndex");

    foreach ( @ipa) {
	#print $_ . "\n";
	($ip, $id) = split  ":", $_;
#	print "$ip at int id $id\n";

	sqlInsert ($dbh, "interfaceips", [ $dev, $ip, $id ] );
    }
}


#GET INTERFACE DESCRIPTIONS
###########################
sqlFlush ($dbh, "interfaces");

foreach (@routers, @switches) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};
    
    print "getting if descriptions from $dev\n";
    @snmpifs =  snmpwalk("$com\@$dev", "ifDescr");

    print "getting if macs from $dev\n";
    %ifmacs = snmphasher ($dev, $com, "ifPhysAddress");    

    foreach (@snmpifs) {
	#print $_ . "\n";
	($id, $desc) = split  ":", $_;
	$mac = hex2mac($ifmacs{$id}) || "";
#	print "mac for $id is $mac\n";

	sqlInsert ($dbh, "interfaces", [ $dev, $id, $desc, $mac ] );
    }
}

#GET ARP TABLES
################
sqlFlush($dbh, "arps");

foreach  (@routers, @switches) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};

    print "gettng arp table from $dev\n";
    %macs = snmphasher ($dev, $com, "ARP");

    foreach (keys %macs ) {
	my ($ifid, $ip) = split /\./, $_, 2;
	sqlInsert($dbh, "arps", [ $dev, $ifid, $ip, hex2mac($macs{$_}) ]);
    }
}

#GET MAC ADDR TABLES
####################
sqlFlush ($dbh, "macs");

foreach  (@switches) {
     $dev = $_->{'dev'};
     $com = $_->{'com'};

     print "getting mac addr table from $dev\n";
    if (iscisco ($dev, $com) ) {
	print "$dev is cisco\n";
	getmacs_cisco ($dev, $com);
    } else {
	print "$dev is not cisco\n";
	getmacs_hp ($dev, $com);
    }
}




# SUBROUTINES
#############

sub iscisco  {
    my ($dev, $com) = @_;

    my @wtf = snmpwalk ("$com\@$dev", "WTF");
    my $f = $wtf[0];
#    print "got $f\n";
    
    ($f =~ /cisco/i) ;
}

sub getmacs_hp { 
    %mac = snmphasher ($dev, $com, "MAC");
    %macport = snmphasher ($dev, $com, "MACPORT"); # actually just need macport

    foreach (keys %macport) {
	sqlInsert ($dbh, "macs", [ $dev, $macport{$_}, int2mac($_)]);
    }

}

sub getmacs_cisco { 
    my ($dev, $com) = @_;

    %vlans = snmphasher ($dev, $com, "Cvlanlist");
#    debughash(\%vlans);
    
    @vlans = map  { @bar = split (/\./, $_); $bar[1] } keys %vlans;

    foreach (@vlans) {	
#	print "vlan: $_\n";
	# %vlanmacs = snmphasher ($dev, "$com\@$_", "MAC");
	%macx2bp =  snmphasher ($dev, "$com\@$_", "Cvlan2bp");
	%ifindex = snmphasher ($dev, $com, "Cbp2if");
	%mac2if = map { int2mac($_) => $ifindex{$macx2bp{$_}} } keys %macx2bp;
	
	foreach (keys %mac2if) {
	    sqlInsert ($dbh, "macs", [ $dev, $mac2if{$_}, $_ ]);
	}
    }
}

sub debughash {
    my %hash = %{$_[0]};

    foreach (keys %hash) {	print "$_ ---->  $hash{$_}\n";    }

}

sub snmphasher {
    my ($dev, $com, $oid) = @_;

    @snmp =  snmpwalk("$com\@$dev", $oid);
 
    return map { split (":",$_); shift @_ => shift @_ } @snmp;
}


sub hex2mac {
    my $hex = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC",$hex));
}

sub int2mac {
    my $int = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC", pack ("CCCCCC", split (/\./, $int))));
}
