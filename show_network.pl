#!/usr/bin/perl -w

$NETWORK=$ARGV[0] || "1.2.3.0/24"; 

use DBI;
require "./my_sql_qs.pl"

sub connect_dbh () {
    my $SQL_USER = "yes_plaintext_oh_no";
    my $SQL_PASSWORD = "yes_plaintext_oh_no";
    $SQL_CONNECT_STRING = "dbi:mysql:host=dev.MY_HOSTNAME.net;database=the_network_db";
    $dbh = DBI->connect ($SQL_CONNECT_STRING, $SQL_USER, $SQL_PASSWORD) || warn $DBI::errstr;
}

connect_dbh();

$LISTCMD = "/usr/bin/nmap -sL -n $NETWORK";
open LIST, "$LISTCMD |" || die "opening $LISTCMD";

while (<LIST>) {
    if (/^host\s+(.*)\s+not scanned/i) {
	push @ips, $1;
    }
}

close LIST;

print "<table>";
print "<tr><td>ip</td><td>arp</td><td>ports</td></tr>\n";

foreach $ip (@ips) {
    print "<tr>";

    @arps = ();
    @ports = ();

    print "<td>$ip</td>";

    @arps = sqlQuery ($dbh, "arps", [ "dev", "ip", "id", "mac" ] , { "ip" => $ip });

    print "<td>";
    print join (", ", map ($_->{"dev"} . " ". getintname ($_->{'dev'}, $_->{'id'}), @arps));
    print "</td>";
    
    print "<td>";
    print join (", ", map ($_->{"dev"} . " " . getintname($_->{'dev'}, $_->{'id'}), sqlQuery ($dbh, "macs", [ "dev", "id", "mac" ], { "mac" => $arps[0]->{'mac'}})));
    print "</td>";

    print "<tr>\n";
    print "\n";
    
}
print "</table>\n\n";


sub getintname {
    my ($dev, $id) = @_;

    @ting = sqlQuery ($dbh, "interfaces", [ "dev", "id", "des" ] , { "dev" => $dev, "id" => $id });

    return $ting[0]->{'des'};
}









$dbh->disconnect();