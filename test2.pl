

@foo = ("foo:as", "bar:gas", "baz:lass", "as", "testes", "a:b:c:d", ":d", "e:");

%hash = map { split (":",$_); shift @_ => shift @_ } @foo;

foreach (keys %hash) {
    print "$_  ---> $hash{$_}\n";
}
