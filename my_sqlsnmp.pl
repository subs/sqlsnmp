#!/usr/bin/perl -w

use DBI;

use lib "/usr/lib/mrtg2/";
use SNMP_util "0.89";
use SNMP_Session "0.57";        # needs map_table

@switches = sqlQuery ($dbh, "switches", ["dev", "com" ]);
@routers = sqlQuery ($dbh, "routers", ["dev", "com" ]);


&snmpmapOID("ARP", "1.3.6.1.2.1.4.22.1.2");
&snmpmapOID("MAC", "1.3.6.1.2.1.17.4.3.1.1");
&snmpmapOID("MACPORT", "1.3.6.1.2.1.17.4.3.1.2");

$Cbase = "1.3.6.1.4.1.9.9.46.1.3.1.1";
&snmpmapOID("Cvlanlist", $Cbase.".2");
&snmpmapOID("Cvlanname", $Cbase.".4");
&snmpmapOID("Cvlan2bp", "1.3.6.1.2.1.17.4.3.1.2");
&snmpmapOID("Cbp2if","1.3.6.1.2.1.17.1.4.1.2");
&snmpmapOID("WTF","1.3.6.1.2.1.1.1");


connect_dbh();

sub iscisco  {
    my ($dev, $com) = @_;

    my @wtf = snmpwalk ("$com\@$dev", "WTF");
    my $f = $wtf[0];
    print "got $f\n";
    
    ($f =~ /cisco/i) ;
}

foreach  (@switches) {
     $dev = $_->{'dev'};
     $com = $_->{'com'};

    if (iscisco ($dev, $com) ) {
	print "$dev is cisco\n";
	getmacs_cisco ($dev, $com);
    } else {
	print "$dev is not cisco\n";
	getmacs_hp ($dev, $com);
    }
}

sub getmacs_hp { 
    %mac = snmphasher ($dev, $com, "MAC");
    %macport = snmphasher ($dev, $com, "MACPORT"); # actually just need macport

    foreach (keys %macport) {
	sqlInsert ($dbh, "macs", [ $dev, $macport{$_}, int2mac($_)]);
    }
}

sub getmacs_cisco { 
    my ($dev, $com) = @_;

    %vlans = snmphasher ($dev, $com, "Cvlanlist");
    debughash(\%vlans);
    
    @vlans = map  { @bar = split (/\./, $_); $bar[1] } keys %vlans;

    foreach (@vlans) {	
	print "vlan: $_\n";
	# %vlanmacs = snmphasher ($dev, "$com\@$_", "MAC");
	%macx2bp =  snmphasher ($dev, "$com\@$_", "Cvlan2bp");
	%ifindex = snmphasher ($dev, $com, "Cbp2if");
	%mac2if = map { int2mac($_) => $ifindex{$macx2bp{$_}} } keys %macx2bp;
	
	foreach (keys %mac2if) {
	    sqlInsert ($dbh, "macs", [ $dev, $mac2if{$_}, $_ ]);
	}
    }
}

sub debughash {
    my %hash = %{$_[0]};

    foreach (keys %hash) {	print "$_ ---->  $hash{$_}\n";    }

}

exit (0);

sqlFlush($dbh, "arps");

foreach  (@routers) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};

    print "getting $dev $com\n";
    %macs = snmphasher ($dev, $com, "ARP");

    foreach (keys %macs ) {
	my ($ifid, $ip) = split /\./, $_, 2;
	sqlInsert($dbh, "arps", [ $dev, $ifid, $ip, hex2mac($macs{$_}) ]);
    }
}



##############
sqlFlush ($dbh, "interfaces");

@res = sqlQuery($dbh, "switches", [ "dev", "com" ]);
push @res, sqlQuery($dbh, "routers", [ "dev", "com" ]);

foreach (@res) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};
    
    print "querying $dev";
    @snmpifs =  snmpwalk("$com\@$dev", "ifDescr");
    
    foreach (@snmpifs) {
	#print $_ . "\n";
	($id, $desc) = split  ":", $_;
	sqlInsert ($dbh, "interfaces", [ $dev, $id, $desc ] );
    }
}




sub snmphasher {
    my ($dev, $com, $oid) = @_;

    @snmp =  snmpwalk("$com\@$dev", $oid);
 
    return map { split (":",$_); shift @_ => shift @_ } @snmp;
}

sub connect_dbh () {
    my $SQL_USER = "yes_plaintext_oh_no";
    my $SQL_PASSWORD = "yes_plaintext_oh_no";
    $SQL_CONNECT_STRING = "dbi:mysql:host=dev.MY_HOSTNAME.net;database=the_network_db";
    $dbh = DBI->connect ($SQL_CONNECT_STRING, $SQL_USER, $SQL_PASSWORD) || warn $DBI::errstr;
}

sub sqlFlush {
    my ($dbh, $table, $criteria) = @_;
    my $sql = "delete FROM $table";
    my $where_clause;
    my @results;

    $where_clause = join (" and ", map ($_ . " = \"" . $$criteria{$_} . "\"",
                                        (keys %$criteria)));

    $sql = $sql . " WHERE " . $where_clause if ($where_clause);

    print $sql . "\n";
    $dbh->do ($sql) or warn "cannot do : " .  $dbh->errstr();
}

sub sqlInsert {
    my ($dbh, $table, $values, $fields) = @_;
    my $sql = "INSERT INTO $table ";
#    my $column_clause = join (", ", @$fields);
    my $value_clause = join (", ", map ( "\'" . $_ . "\'", @$values));
    
#   $sql .= "($column_clause) " if ($fields);
    $sql .= "VALUES ($value_clause) ";
    
    print $sql . "\n";
    $dbh->do($sql) or warn "cannot do : " .  $dbh->errstr();
}   
 
    
sub sqlQuery {

    my ($dbh, $table, $fields, $criteria) = @_;
    my $field_list = join (",", @$fields);
    my $sql = "SELECT $field_list FROM $table";
    my $where_clause;
    my @results;

    $where_clause = join (" and ", map ($_ . " = \"" . $$criteria{$_} . "\"",
                                        (keys %$criteria)));

    $sql = $sql . " WHERE " . $where_clause if ($where_clause);

#    print $sql;

    my $sth = $dbh->prepare($sql) || warn $DBH->errstr();

    $num_rows = $sth->execute() || warn $sth->errstr();

#use fetchall_arrayref??
    while ($row = $sth->fetchrow_hashref() ) {
        push @results, $row;
    }

    $sth->finish();

    return @results;
}

sub hex2mac {
    my $hex = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC",$hex));
}


sub int2mac {
    my $int = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC", pack ("CCCCCC", split (/\./, $int))));
}
