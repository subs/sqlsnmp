#!/usr/bin/perl -w

require "my_sql_qs.pl"

use lib "/usr/lib/mrtg2/";
use SNMP_util "0.89";
use SNMP_Session "0.57";        # needs map_table

$dev = $ARGV[0] || die &usage;
$com = $ARGV[1] || die &usage;

$TYPE_ETHERNET = 6;
$STATUS_UP = 1;
$STATUS_DOWN = 2;

%adminStatus = snmphasher ($dev, $com, "ifAdminStatus");
%operStatus = snmphasher ($dev, $com, "ifOperStatus");
%speed = snmphasher ($dev, $com, "ifSpeed");
%desc = snmphasher ($dev, $com, "ifDescr");

print "<table>";

print "<tr><td>description</td><td>speed</td><td>oper status</td><td>admin status</td></tr>";
foreach (sort keys %desc) {
    $status = ($operStatus{$_} == $STATUS_UP ? "UP" : "DOWN");
    $adstatus = ($adminStatus{$_} == $STATUS_UP ? "UP" : "DOWN");
    $speedo = ($speed{$_} / 1000000) ." mbps";
    print "<tr> <td> $desc{$_} </td><td> $speedo </td><td> $status </td> <td> $adstatus </td> </tr>\n";
}

print "</table>";


sub usage () {
    print "$0: <device> <community-string>\n";
}


sub snmphasher {
    my ($dev, $com, $oid) = @_;

    @snmp =  snmpwalk("$com\@$dev", $oid);
 
    return map { split (":",$_); shift @_ => shift @_ } @snmp;
}


