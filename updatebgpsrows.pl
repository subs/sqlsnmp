#!/usr/bin/perl -w

require "my_sql_qs.pl";

use DBI;

use lib "/usr/lib/mrtg2/";
use SNMP_util "0.89";
use SNMP_Session "0.57";        # needs map_table


&snmpmapOID("AS", 1.3.6.1.2.1.15.2.0);
&snmpmapOID("peerstate", 1.3.6.1.2.1.15.3.1.2);
&snmpmapOID("remoteAS", 1.3.6.1.2.1.15.3.1.9);

sub connect_dbh () {
    my $SQL_USER = "yes_plaintext_oh_no";
    my $SQL_PASSWORD = "yes_plaintext_oh_no";
    $SQL_CONNECT_STRING = "dbi:mysql:host=dev.MY_HOSTNAME.net;database=the_network_db";
    $dbh = DBI->connect ($SQL_CONNECT_STRING, $SQL_USER, $SQL_PASSWORD) || warn $DBI::errstr;
}

connect_dbh();

@routers = sqlQuery ($dbh, "routers", ["dev", "com" ]);

#GET BGP
################
sqlFlush($dbh, "bgp");

foreach  (@routers) {
    $dev = $_->{'dev'};
    $com = $_->{'com'};
    
    print "gettng bgp table from $dev\n";
    %localass = snmphasher ($dev, $com, "1.3.6.1.2.1.15.2");
    $locas = $localass{0};
#    print "got locas: $locas\n";

#    next;

    %peers = snmphasher ($dev, $com,  "1.3.6.1.2.1.15.3.1.2");
    %localips = snmphasher2 ($dev, $com , "1.3.6.1.2.1.15.3.1.5");
    %remoteas = snmphasher ($dev, $com , "1.3.6.1.2.1.15.3.1.9");
    

    foreach (keys %peers ) {
	$locip = $localips {$_};
	$state = $peers{$_};
	$remas = $remoteas{$_};
#	print "peer ip: $locip (local) <--> $_ ($remas)\tstate: $state\n";
	#print "$_\n";
	sqlInsert($dbh, "bgp", [ $dev, $locip, $locas, $_, $remas, $state] );
    }
}

# SUBROUTINES
#############

sub iscisco  {
    my ($dev, $com) = @_;

    my @wtf = snmpwalk ("$com\@$dev", "WTF");
    my $f = $wtf[0];
#    print "got $f\n";
    
    ($f =~ /cisco/i) ;
}



sub debughash {
    my %hash = %{$_[0]};

    foreach (keys %hash) {	print "$_ ---->  $hash{$_}\n";    }

}

sub snmphasher {
    my ($dev, $com, $oid) = @_;

    @snmp =  snmpwalk("$com\@$dev", $oid);
 
    return map { split (":",$_); shift @_ => shift @_ } @snmp;
}

sub snmphasher2 {
    my ($dev, $com, $oid) = @_;

    @snmp =  snmpwalk("$com\@$dev", $oid);
 
    return map { split (":",$_);  $_[0] =>  $_[1] } @snmp;
}


sub hex2mac {
    my $hex = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC",$hex));
}

sub int2mac {
    my $int = shift;
    return sprintf ("%02x:%02x:%02x:%02x:%02x:%02x", unpack ("CCCCCC", pack ("CCCCCC", split (/\./, $int))));
}
