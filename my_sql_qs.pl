#!/usr/bin/perl -w

#functions that someone else probably already wrote do do elementary sqls

sub sqlFlush {
    my ($dbh, $table, $criteria) = @_;
    my $sql = "delete FROM $table";
    my $where_clause;
    my @results;

    $where_clause = join (" and ", map ($_ . " = \"" . $$criteria{$_} . "\"",
                                        (keys %$criteria)));

    $sql = $sql . " WHERE " . $where_clause if ($where_clause);

   # print $sql . "\n";
    $dbh->do ($sql) or warn "cannot do : " .  $dbh->errstr();
}

sub sqlInsert {
    my ($dbh, $table, $values, $fields) = @_;
    my $sql = "INSERT INTO $table ";
#    my $column_clause = join (", ", @$fields);
    my $value_clause = join (", ", map ( "\'" . $_ . "\'", @$values));
    
#   $sql .= "($column_clause) " if ($fields);
    $sql .= "VALUES ($value_clause) ";
    
  #  print $sql . "\n";
    $dbh->do($sql) or warn "cannot do : " .  $dbh->errstr();
}   
 


    
sub sqlQuery {

    my ($dbh, $table, $fields, $criteria) = @_;
    my $field_list = join (",", @$fields);
    my $sql = "SELECT $field_list FROM $table";
    my $where_clause;
    my @results;

    $where_clause = join (" and ", map ($_ . " = \'" . $$criteria{$_} . "\'",
                                        (keys %$criteria)));

    $sql = $sql . " WHERE " . $where_clause if ($where_clause);

#    print $sql;

    my $sth = $dbh->prepare($sql) || warn $DBH->errstr();

    $num_rows = $sth->execute() || warn $sth->errstr();

#use fetchall_arrayref??
    while ($row = $sth->fetchrow_hashref() ) {
        push @results, $row;
    }

    $sth->finish();

    return @results;
}


1;
